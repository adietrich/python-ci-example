# python-ci-example

Example project for running a Python script in a [CI/CD pipeline](https://docs.gitlab.com/ee/ci/):

- Runs the script only in manual or scheduled pipelines.
- Uses [pyenv](https://github.com/pyenv/pyenv) for managing the local Python version.
- Uses [Pipenv](https://pipenv.pypa.io/) for managing Python dependencies.

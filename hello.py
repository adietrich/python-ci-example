#!/usr/bin/env python3

import os

import cowsay


GREETING = os.environ.get('GREETING', 'Hello, World!')


if __name__ == '__main__':
    cowsay.cow(GREETING)
